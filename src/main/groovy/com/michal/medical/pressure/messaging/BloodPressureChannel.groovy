package com.michal.medical.pressure.messaging

import org.springframework.cloud.stream.annotation.Input
import org.springframework.messaging.SubscribableChannel

interface BloodPressureChannel {

    @Input
    SubscribableChannel producer()

}