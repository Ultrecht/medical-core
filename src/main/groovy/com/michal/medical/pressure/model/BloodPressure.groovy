package com.michal.medical.pressure.model

import java.util.function.Predicate

class BloodPressure {

    /**
     * Cisnienie skurczowe
     */
    private final int systolic

    /**
     * Cisnienie rozkurczowe
     */
    private final int diastolic

    private final int pulse

    private BloodPressure(int systolic, int diastolic, int pulse) {
        this.systolic = systolic
        this.diastolic = diastolic
        this.pulse = pulse
    }

    int systolic() {
        systolic
    }

    int diastolic() {
        diastolic
    }

    int pulse() {
        pulse
    }

    static BloodPressure of(int systolic, int diastolic, int pulse) {
        new BloodPressure(systolic, diastolic, pulse)
    }

    boolean isInRange(Predicate<BloodPressure> predicate) {
        predicate.test(this)
    }

    @Override
    String toString() {
        "Ciśnienie $systolic na $diastolic puls $pulse"
    }

}
