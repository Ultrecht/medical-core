package com.michal.medical.pressure.model

enum Position {

    UPRIGHT("UP", "upright"),
    HORIZONTAL("HO", "horizontal"),
    SEATED("SE", "seated"),
    RECLINED("RE", "reclined")

    private String dbCode;
    private String labelProperty;

    Position(String dbCode, String labelProperty) {
        this.dbCode = dbCode;
        this.labelProperty = labelProperty;
    }
}