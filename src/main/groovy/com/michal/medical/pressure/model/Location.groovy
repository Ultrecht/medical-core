package com.michal.medical.pressure.model

enum Location {

    LEFT_WRIST("LW", "left_wrist"),
    RIGHT_WRIST("RW", "right_wrist"),
    LEFT_ARM("LA", "left_arm"),
    RIGHT_ARM("RA", "right_arm"),
    LEFT_LEG("LL", "left_leg"),
    RIGHT_LEG("RL", "riht_leg")

    private String dbCode
    private String labelProperty

    Location(String dbCode, String labelProperty) {
        this.dbCode = dbCode
        this.labelProperty = labelProperty
    }
}