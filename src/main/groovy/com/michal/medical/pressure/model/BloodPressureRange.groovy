package com.michal.medical.pressure.model

import java.util.function.Predicate

class BloodPressureRange {

    static Predicate<BloodPressure> low() {
        new LowBPRange()
    }

    static Predicate<BloodPressure> ideal() {
        new IdealBPRange()
    }

    static Predicate<BloodPressure> preHigh() {
        new PreHighBPRange();
    }

    static Predicate<BloodPressure> high() {
        new HighBPRange()
    }

    static class LowBPRange implements Predicate<BloodPressure> {

        PressureRange systolic = new PressureRange(90, 60)
        PressureRange diastolic = new PressureRange(60, 40)

        @Override
        boolean test(BloodPressure bp) {
            systolic.inRange(bp.systolic()) && diastolic.inRange(bp.diastolic())
        }
    }

    static class IdealBPRange implements Predicate<BloodPressure> {

        PressureRange systolicA = new PressureRange(120, 90)
        PressureRange diastolicA = new PressureRange(80, 40)
        PressureRange systolicB = new PressureRange(120, 70)
        PressureRange diastolicB = new PressureRange(80, 60)

        @Override
        boolean test(BloodPressure bp) {
            (systolicA.inRange(bp.systolic()) && diastolicA.inRange(bp.diastolic())) ||
                    (systolicB.inRange(bp.systolic()) && diastolicB.inRange(bp.diastolic()))
        }
    }

    static class PreHighBPRange implements Predicate<BloodPressure> {

        PressureRange systolicA = new PressureRange(140, 120)
        PressureRange diastolicA = new PressureRange(90, 40)
        PressureRange systolicB = new PressureRange(140, 70)
        PressureRange diastolicB = new PressureRange(90, 80)

        @Override
        boolean test(BloodPressure bp) {
            (systolicA.inRange(bp.systolic()) && diastolicA.inRange(bp.diastolic())) ||
                    (systolicB.inRange(bp.systolic()) && diastolicB.inRange(bp.diastolic()))
        }
    }

    static class HighBPRange implements Predicate<BloodPressure> {

        PressureRange systolicA = new PressureRange(190, 140)
        PressureRange diastolicA = new PressureRange(100, 40)
        PressureRange systolicB = new PressureRange(190, 70)
        PressureRange diastolicB = new PressureRange(100, 90)

        @Override
        boolean test(BloodPressure bp) {
            (systolicA.inRange(bp.systolic()) && diastolicA.inRange(bp.diastolic())) ||
                    (systolicB.inRange(bp.systolic()) && diastolicB.inRange(bp.diastolic()))
        }
    }

    static class PressureRange {

        int upper
        int lower

        PressureRange(int upper, int lower) {
            this.upper = upper
            this.lower = lower
        }

        int getUpper() {
            upper
        }

        int getLower() {
            lower
        }

        boolean inRange(int value) {
            value <= upper && value >= lower
        }
    }

}
