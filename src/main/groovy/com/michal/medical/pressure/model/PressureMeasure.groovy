package com.michal.medical.pressure.model

import java.time.LocalDateTime

class PressureMeasure {

    private final LocalDateTime measureDate
    private final BloodPressure bloodPressure
    private final String description
    private final Location location
    private final Position position

    PressureMeasure(LocalDateTime measureDate, BloodPressure bloodPressure, String description, Location location, Position position) {
        this.measureDate = measureDate
        this.bloodPressure = bloodPressure
        this.description = description
        this.location = location
        this.position = position
    }

    LocalDateTime getMeasureDate() {
        measureDate
    }

    BloodPressure getBloodPressure() {
        bloodPressure
    }

    String getDescription() {
        description
    }

    Location getLocation() {
        location
    }

    Position getPosition() {
        position
    }

    @Override
    String toString() {
        "$measureDate $bloodPressure $description $location $position"
    }

    @Override
    boolean equals(Object o) {
        if (this == o) {
            return true
        }
        if (o == null || getClass() != o.getClass()) {
            return false
        }

        PressureMeasure that = (PressureMeasure) o

        measureDate == that.measureDate
    }

    @Override
    int hashCode() {
        measureDate.hashCode()
    }

}
