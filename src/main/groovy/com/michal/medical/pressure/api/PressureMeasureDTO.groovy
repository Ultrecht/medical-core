package com.michal.medical.pressure.api

import groovy.transform.Canonical

import javax.validation.constraints.NotNull

import com.michal.medical.pressure.model.Location
import com.michal.medical.pressure.model.Position

@Canonical
class PressureMeasureDTO {

    @NotNull
    Date measureDate

    @NotNull
    BloodPressureDTO bloodPressure

    String description

    Location location

    Position position

}
