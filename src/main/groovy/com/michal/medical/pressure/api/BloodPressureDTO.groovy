package com.michal.medical.pressure.api

import groovy.transform.Canonical

import javax.validation.constraints.NotNull

@Canonical
class BloodPressureDTO {

    @NotNull
    Integer systolic

    @NotNull
    Integer diastolic

    @NotNull
    Integer pulse

}
