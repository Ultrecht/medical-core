package com.michal.medical.pressure.resources

import groovy.util.logging.Slf4j

import javax.validation.Valid

import com.michal.medical.pressure.api.PressureMeasureDTO
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = '/api/v1/pressure',
        produces = MediaType.APPLICATION_JSON_VALUE,
        consumes = MediaType.APPLICATION_JSON_VALUE)
@Slf4j
class PressureMeasureResource {

    @RequestMapping(method = RequestMethod.POST)
    void save(@Valid @RequestBody PressureMeasureDTO dto) {
        log.info('Saving blood pressure')
    }

}
