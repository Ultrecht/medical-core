package com.michal.medical

import java.util.logging.Logger

import com.michal.medical.pressure.messaging.BloodPressureChannel
import org.springframework.beans.factory.InjectionPoint
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.cloud.stream.annotation.EnableBinding
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Scope
import org.springframework.integration.dsl.IntegrationFlow
import org.springframework.integration.dsl.IntegrationFlows

@EnableBinding(BloodPressureChannel)
@SpringBootApplication
class MedicalApplication {

    @Bean
    @Scope("prototype")
    Logger getLogger(InjectionPoint ij) {
        Logger.getLogger(ij.declaredType.name)
    }

    static void main(String[] args) {
        SpringApplication.run(MedicalApplication.class, args)
    }

    @Bean
    IntegrationFlow integrationFlow(Logger logger,BloodPressureChannel channel) {
        IntegrationFlows.from(channel.producer()).handle(String.class,
                { payload, header ->
                    logger.info("is this working $payload")
                    null }).get()
    }

}
