package com.michal.medical.pressure.util

import com.michal.medical.pressure.api.BloodPressureDTO

class BloodPressureDTOBuilder {

    private Integer systolic = 120
    private Integer diastolic = 80
    private Integer pulse = 80

    static BloodPressureDTO idealBP() {
        new BloodPressureDTO(systolic: 120, diastolic: 80, pulse: 80)
    }

    static BloodPressureDTOBuilder defaultBloodPressure() {
        new BloodPressureDTOBuilder()
    }

    def systolic(systolic) {
        this.systolic = systolic
        this
    }

    def diastolic(diastolic) {
        this.diastolic = diastolic
        this
    }

    def pulse(pulse) {
        this.pulse = pulse
        this
    }

    def build() {
        new BloodPressureDTO(systolic: systolic, diastolic: diastolic, pulse: pulse)
    }

}
