package com.michal.medical.pressure.util

import groovy.json.JsonOutput

import com.michal.medical.pressure.api.BloodPressureDTO
import com.michal.medical.pressure.api.PressureMeasureDTO
import com.michal.medical.pressure.model.Location
import com.michal.medical.pressure.model.Position

class PressureMeasureDTOBuilder {

    private Date measureDate = new Date()
    private BloodPressureDTO bloodPressure = BloodPressureDTOBuilder.idealBP()
    private String description
    private Location location
    private Position position

    static PressureMeasureDTOBuilder defaultPressureMeasure() {
        new PressureMeasureDTOBuilder()
    }

    static PressureMeasureDTOBuilder emptyPressureMeasure() {
        new PressureMeasureDTOBuilder(measureDate: null, bloodPressure: null)
    }

    def measureDate(measureDate) {
        this.measureDate = measureDate
        this
    }

    def bloodPressure(BloodPressureDTOBuilder builder) {
        this.bloodPressure = builder.build()
        this
    }

    def bloodPressure(BloodPressureDTO dto) {
        this.bloodPressure = dto
        this
    }

    def description(description) {
        this.description = description
        this
    }

    def location(location) {
        this.location = location
        this
    }

    def position(position) {
        this.position = position
        this
    }

    def build() {
        new PressureMeasureDTO(measureDate: measureDate,
                bloodPressure: bloodPressure,
                description: description,
                location: location,
                position: position)
    }

    def asJson() {
        JsonOutput.toJson(build())
    }

}
