package com.michal.medical.pressure.events

import groovyx.net.http.HttpResponseDecorator
import groovyx.net.http.RESTClient

import com.michal.medical.MedicalApplication
import org.springframework.boot.context.embedded.LocalServerPort
import org.springframework.boot.test.SpringApplicationConfiguration
import org.springframework.boot.test.WebIntegrationTest
import spock.lang.Specification
import spock.lang.Unroll

import static com.michal.medical.pressure.util.BloodPressureDTOBuilder.defaultBloodPressure
import static com.michal.medical.pressure.util.BloodPressureDTOBuilder.incorectBloodPressure
import static com.michal.medical.pressure.util.PressureMeasureDTOBuilder.defaultPressureMeasure
import static com.michal.medical.pressure.util.PressureMeasureDTOBuilder.emptyPressureMeasure
import static groovyx.net.http.ContentType.JSON

// TODO: Change to SpringBootTest, after bumping spock version to 1.1
@SpringApplicationConfiguration(classes = [MedicalApplication.class])
@WebIntegrationTest(randomPort = true)
class BloodPressureMeasurementIT extends Specification {

    @LocalServerPort
    int port

    RESTClient client

    def setup() {
        client = new RESTClient("http://localhost:$port/api/v1/pressure")
        client.handler.failure = client.handler.success
    }

    @Unroll
    "Should return 404 when validation fails"() {
        given: 'A empty pressure measure'
        def dto = pressureMeasure.asJson()

        when: 'Try to save measure'
        def result = client.post(
                body: dto,
                contentType: JSON) as HttpResponseDecorator

        then: 'Should return bad request status'
        result.status == 400

        where:
        pressureMeasure << [emptyPressureMeasure()]
    }

    def "Should pass validation"() {
        given: 'A valid pressure measure'
        def dto = defaultPressureMeasure().asJson()

        when: 'Try to save measure'
        def result = client.post(
                body: dto,
                contentType: JSON) as HttpResponseDecorator

        then: 'Should return ok status'
        result.status == 200
    }
}
